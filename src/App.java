import controller.Controller;
import model.Model;
import view.view;

public class App {
    public static void main(String[] args) {
        Model model = new Model();
        System.out.println("model created");
        view view = new view();
        Controller controller = new Controller();
    }
}